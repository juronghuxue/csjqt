//发布上线时注释互换

'use strict'

const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
    NODE_ENV: '"production"',
    //  API_ROOT: '"http://192.168.1.194:8181/handler/"'   //线上请求前缀
    // API_ROOT: '"http://192.168.8.148:8181/handler/"'   //线上请求前缀
    // API_ROOT: '"http://192.168.8.148:8182/handler/"'   //生产环境下的测试环境
    // API_ROOT: '"http://47.100.104.82:8181/handler/"'   //线上请求前缀
    // API_ROOT: '"http://192.168.1.196:8999"'
    // API_ROOT: '"http://139.9.126.117:8999"',
    API_ROOT: '"http://211.95.41.112:8999"',
    // API_ROOT: '"http://192.168.0.91:8999"',
    // API_ROOT: '"http://101.231.140.106:8999"'
})

// 'use strict'
// module.exports = {
//     NODE_ENV: '"production"'
// }