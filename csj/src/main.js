//The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'element-ui/lib/theme-chalk/index.css'
import 'element-ui/lib/theme-chalk/display.css'
import 'babel-polyfill'
import promise from 'es6-promise'
promise.polyfill()
import "./less/main.css";
import Vue from 'vue'
import Vuex from 'vuex'
import ElementUI from 'element-ui'
import App from './App'
import router from './router'
import store from './store'
import axios from 'axios'
import request from './ajax/ajax'
import echarts from 'echarts'
import './assets/iconfont/iconfont.css'
import './assets/iconfont2/iconfont.css'
import './assets/dataManageIcon/iconfont.css';
import BaiduMap from 'vue-baidu-map'
Vue.use(Vuex)
Vue.use(axios)
Vue.use(ElementUI, { size: 'medium' });
Vue.config.productionTip = false
Vue.prototype.$get = request.get
Vue.prototype.$post = request.post
Vue.prototype.$copyPost = request.copyPost
Vue.prototype.$uploadFile = request.uploadFile
Vue.prototype.$getFile = request.getFile
Vue.prototype.thisVue = this
Vue.prototype.$echarts = echarts
Vue.prototype.$axios = axios
Vue.use(BaiduMap, {
    ak: '6brZ2DMcU4AIsgZ5cGErr1ct9tZ3G7Dq'
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
});
