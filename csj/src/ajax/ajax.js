
//引入axios
import axios from 'axios'
import qs from 'qs'
import store from '../store'
import router from '../router'
var root = process.env.API_ROOT;
import { Message } from "element-ui";
import { Loading } from 'element-ui'

var _this = this
let cancel ,promiseArr = {}
const CancelToken = axios.CancelToken;
//请求拦截器
// axios.interceptors.request.use(config => {
//     //发布上线时开放下面两行
//     config.url = root + config.url;
//     return config;
//     //发起请求时，取消掉当前正在进行的相同请求
//     // if (promiseArr[config.url]) {
//     //     promiseArr[config.url]('操作取消')
//     //     promiseArr[config.url] = cancel
//     // } else {
//     //     promiseArr[config.url] = cancel
//     // }
//     // return config
// }, error => {
//     //return Promise.reject(error)
// })

//响应拦截器即异常处理
axios.interceptors.response.use(response => {
    // store.dispatch('handleShowError',[false,'']);
    if(response.data.success){
        return response
    }else{
        // store.dispatch('handleShowError',[true,response.data.errorMsg]);
        return response;
    }
}, err => {
    if (err && err.response) {
        switch (err.response.status) {
            case 400:
                err.message = '错误请求'
                break;
            case 401:
                err.message = '未授权，请重新登录'
                break;
            case 403:
                err.message = '拒绝访问'
                break;
            case 404:
                //store.dispatch('handleShowError',[true,'请求错误,未找到该资源']);
                //err.message = '请求错误,未找到该资源'
                break;
            case 405:
                err.message = '请求方法未允许'
                break;
            case 408:
                //store.dispatch('handleShowError',[true,'请求超时']);
                //err.message = '请求超时'
                break;
            case 500:
                //store.dispatch('handleShowError',[true,'服务器端出错']);
                //err.message = '服务器端出错'
                break;
            case 501:
                err.message = '网络未实现'
                break;
            case 502:
                err.message = '网络错误'
                break;
            case 503:
                err.message = '服务不可用'
                break;
            case 504:
                err.message = '网络超时'
                break;
            case 505:
                err.message = 'http版本不支持该请求'
                break;
            default:
                err.message = `连接错误${err.response.status}`
        }
    } else {
        err.message = "连接到服务器失败"
    }
    // message.err(err.message)
    return Promise.resolve(err.response)
})

if (process.env.NODE_ENV == 'development') {
    axios.defaults.baseURL = '/api'
} else if (process.env.NODE_ENV == 'debug') {
    axios.defaults.baseURL = null
} else if (process.env.NODE_ENV == 'production') {
    axios.defaults.baseURL = null
}



// axios.defaults.baseURL = '/api'
//设置默认请求头
axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded'
axios.defaults.timeout = 60000

export default {
    //get请求
    get (url,param) {
        return new Promise((resolve,reject) => {
            axios({
                method: 'get',
                url,
                params: qs.stringify(param),
                cancelToken: new CancelToken(c => {
                    cancel = c
                })
            }).then(res => {
                if(res.data.errorCode == '201'){
                    router.push('/login');
                }else{
                    resolve(res);
                }
            })
        })
    },
    //post请求
    post (url,param) {
        let loading = Loading.service({
            lock: true,
            text: '正在加载中',
            spinner: 'el-icon-loading',
            background: 'rgba(0, 0, 0, 0.7)'
        })
        return new Promise((resolve,reject) => {
            axios({
                method: 'post',
                url,
                data: qs.stringify(param),
                dataType:'json',
                cancelToken: new CancelToken(c => {
                    cancel = c
                })
            }).then(res => {
                loading.close()
                if(res.data.errorCode == '201'){
                    router.push('/login');
                }else{
                    resolve(res);
                }
            })
        })
    },
    copyPost(url,params){
        let loading = Loading.service({
            lock: true,
            text: '正在加载中',
            spinner: 'el-icon-loading',
            background: 'rgba(0, 0, 0, 0.7)'
        })
        return new Promise((resolve, reject) => {
            axios.post(`${url}`, params, { headers: { 'Content-Type': 'application/json;charset=UTF-8' } })
                .then(res => {
                    loading.close()
                    if (res.data.success) {
                        resolve(res.data)
                    } else {
                        Message.error(res.data.errorMsg)
                    }
                })
                .catch(err => {
                    loading.close()
                    reject(err.data)
                })
        })
    },
    uploadFile(url,params){
        let loading = Loading.service({
            lock: true,
            text: '正在加载中',
            spinner: 'el-icon-loading',
            background: 'rgba(0, 0, 0, 0.7)'
        })
        return new Promise( (resolve, reject) => {
            axios.post(`${url}`, params, { headers: {'Content-Type': 'multipart/form-data'}})
                .then(res =>{
                    loading.close()
                    if(res.data.success){
                        resolve(res.data)
                    }else{
                        // Message.info('请联系后台')
                        Message.error(res.data.errorMsg)
                    }
                })
                .catch(err =>{
                    loading.close()
                    reject(err.data)
                })
        })
    },
    getFile(url,params){
        let loading = Loading.service({
            lock: true,
            text: '正在加载中',
            spinner: 'el-icon-loading',
            background: 'rgba(0, 0, 0, 0.7)'
        })
        return new Promise( (resolve, reject) => {
            axios.post(`${url}`, params, { responseType: 'blob'})
                .then(res =>{
                    loading.close()
                    if(res.data){
                        resolve(res)
                    }else{
                        // Message.info('请联系后台')
                        Message.error(res.data.errorMsg)
                    }
                })
                .catch(err =>{
                    loading.close()
                    reject(err.data)
                })
        })
    }
}
