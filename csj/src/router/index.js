import Vue from 'vue'
import Router from 'vue-router'
import Login from '../components/common/login'
import registerSuccess from '../components/common/registerSuccess'//注册成功
import resetPassword from '../components/common/resetPassword'//修改密码
import Home from '@/components/common/home'
import xmpjSider from '@/components/common/xmpjSider'
import dysjSider from '@/components/common/dysjSider'//多元数据导航栏
import cxsfxm from '@/components/projectML/cxsfxm'//创新示范项目
import cxdt from '@/components/dynamicInnovation/cxdt'//创新动态
import njsj from '@/components/projectML/njsj'//年鉴数据



import projectMLdetails from '@/components/projectML/details'//数据管理- 进度考核详情
import hadoop from '@/components/projectML/hadoop'//多元数据-大数据分析
import kshgz from '@/components/projectML/kshgz'//可视化跟踪
import dsjzbsj from '@/components/projectML/dsjzbsj'//可视化跟踪
import cydsjfx from '@/components/projectML/cydsjfx'//可视化跟踪

import checkList from '@/components/projectML/checkList'
import pingjiaManage from '@/components/projectML/pingjiaManage'
import pingjiaRecord from '@/components/projectML/pingjiaRecord'
import areaPingjia from '@/components/projectML/areaPingjia'
import tongji from '@/components/projectML/tongji'
import fenxi from '@/components/projectML/fenxi'
import fzcx from '@/components/xmpgmx/fzcx'//区域发展与创新能力评估
import cyfzPg from '@/components/xmpgmx/cyfzPg'//产业发展与创新能力评估
import cggz from '@/components/xmpgmx/cggz'//成果跟踪
import qyzbsjgl from '@/components/xmpgmx/qyzbsjgl'//企业直报数据管理
import zbqd from '@/components/xmpgmx/zbqd'//企业直报数据管理-直报清单
import qyzbsjtjt from '@/components/xmpgmx/qyzbsjtjt'//企业直报数据管理-区域直报数据统计图
import qiyezbsjtjt from '@/components/xmpgmx/qiyezbsjtjt'//企业直报数据管理-企业直报数据统计图
import qidong from '@/components/projectML/qidong'
import dongtaigenzong from '@/components/projectML/dongtaigenzong'
import xmpjgl from '@/components/projectML/xmpjgl'
import ckpsxq from '@/components/projectML/ckpsxq'//查看评审详情
import xmpjgltjt from '@/components/projectML/xmpjgltjt'//项目评价管理统计图
import xmpjgltjtpt from '@/components/projectML/xmpjgltjtpt'//项目评价管理统计图
import xmjdhb from '@/components/projectML/xmjdhb'//项目动态监测
import xmjdhb2 from '@/components/projectML/xmjdhb2'//项目进度汇报
import xmjdhb3 from '@/components/projectML/xmjdhb3'//项目进度汇报
import pgRecord from '@/components/projectML/pgRecord'
import qyztpjgl from '@/components/projectML/qyztpjgl'
import qypgxq from '@/components/projectML/qypgxq'
import xmqd from '@/components/projectML/xmqd'
import zjdf from '@/components/projectML/zjdf'
import qypjmx from '@/components/projectML/qypjmx'
import achievementgenzong from '@/components/achievement/achievementgenzong'
import qyjxmb from '@/components/projectML/qyjxmb'
import downloadArea from '@/components/projectML/downloadArea'//项目管理-下载专区

import dataSider from '@/components/common/dataSider'
import xmlb from '@/components/dataManage/xmlb'//数据管理-项目列表
import wsxx from '@/components/dataManage/wsxx'//数据管理-项目列表-完善信息
import xmxq from '@/components/dataManage/xmxq'//数据管理-项目列表-项目详情
import xmxxsh from '@/components/dataManage/xmxxsh'//数据管理-项目信息审核
import tjxmjdrw from '@/components/dataManage/tjxmjdrw'//数据管理-添加项目进度任务
import zjtx from '@/components/dataManage/zjtx'//在线填报-填报项目进度
import fzdxm from '@/components/dataManage/fzdxm'//在线填报-负责的项目
import qysqgl from '@/components/dataManage/qysqgl'//在线填报-负责的项目
import onlineZhibao from '@/components/dataManage/onlineZhibao'//企业直报数据
import processData from '@/components/dataManage/processData'//项目进度汇报数据
import dataXmml from '@/components/dataManage/dataXmml'//数据管理-项目管理
import cjxm from '@/components/dataManage/cjxm'//数据管理-项目管理
import shenpiProcessData from '@/components/dataManage/shenpiProcessData'//数据管理-项目管理
import dataCggz from '@/components/dataManage/dataCggz'//数据管理-成果跟踪
import yszlsp from '@/components/dataManage/yszlsp'//验收资料审批
import ckzl from '@/components/dataManage/ckzl'//数据审批-验收资料审批-查看资料
import qiyeyszlsp from '@/components/dataManage/qiyeyszlsp'//在线填报-验收资料审批-查看资料
import qyjxmbsp from '@/components/dataManage/qyjxmbsp'//数据审批-区域绩效目标审批
import tbcggz from '@/components/dataManage/tbcggz'//企业填报成果跟踪
import tjcggz from '@/components/dataManage/tjcggz'//添加成果跟踪
import wjgl from '@/components/dataManage/wjgl'//添加成果跟踪
import qywjgl from '@/components/dataManage/qywjgl'//添加成果跟踪

import details from '@/components/dataManage/details'//数据管理- 进度考核详情

import video from '@/components/projectML/video'//数据管理- 进度考核详情

import cxcgsy from '@/components/cxcgjy/cxcgsy'//创新成果交易
import cxcgjyDetail from '@/components/cxcgjy/detail'//创新成果交易

import visualization from '@/components/Track/Visualization'
import WholeAreaEvaluate from '@/components/ProjectManage/WholeAreaEvaluate'
import ProductVerify from '@/components/ProductVerify'
import AddExpert from '@/components/Expert/AddExpert'

import notice from '@/components/notice'

import productList from '@/components/ProductList'
import Element from 'element-ui';
import { Message } from 'element-ui'

import processDetail from '@/components/ProjectManage/ProcessDetail'

import $ from 'jquery';
// Vue.use(Message);
Vue.use(Element);

Vue.use(Router);
Vue.prototype.$message = Message;
const router = new Router({
    routes: [
        {
            path: '/',
            name: 'Login',
            component: Login
        },
        {
            path: '/video',
            name: 'video',
            component: video
        },
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/registerSuccess',
            name: 'registerSuccess',
            component: registerSuccess
        },
        {
            path: '/resetPassword',
            name: 'resetPassword',
            component: resetPassword
        },
        {
            path: '/projectML',
            name: 'Home',
            component: Home,
            children: [
                {
                    path: '/projectML/xiangmuProcess',
                    name: 'xmpjSider',
                    component: xmpjSider,
                    children:[
                        {
                            path: '/projectML/xiangmuProcess/processDetail',
                            name: 'processDetail',
                            component: processDetail
                        },
                        {
                            path: '/projectML/xiangmuProcess/xmpjgl',
                            name: 'xmpjgl',
                            component: xmpjgl,
                        },
                        {
                            path: '/projectML/xiangmuProcess/WholeAreaEvaluate',
                            name: 'WholeAreaEvaluate',
                            component: WholeAreaEvaluate
                        },
                        {
                            path: '/projectML/xiangmuProcess/ckpsxq',
                            name: 'ckpsxq',
                            component: ckpsxq,
                        },
                        {
                            path: '/projectML/xiangmuProcess/xmpjgltjt',//项目评价管理-项目评估结果统计图
                            name: 'xmpjgltjt',
                            component: xmpjgltjt,
                        },
                        {
                            path: '/projectML/xiangmuProcess/xmpjgltjtpt',//项目评价管理-项目评估结果统计图-平台
                            name: 'xmpjgltjtpt',
                            component: xmpjgltjtpt,
                        },
                        {
                            path: '/projectML/xiangmuProcess/xmjdhb',//项目动态监测
                            name: 'xmjdhb',
                            component: xmjdhb,
                        },
                        {
                            path: '/projectML/xiangmuProcess/xmjdhb2',//项目进度汇报
                            name: 'xmjdhb2',
                            component: xmjdhb2,
                        },
                        {
                            path: '/projectML/xiangmuProcess/xmjdhb3',//项目进度汇报
                            name: 'xmjdhb3',
                            component: xmjdhb3,
                        },
                        {
                            path: '/projectML/xiangmuProcess/downloadArea',//下载专区
                            name: 'downloadArea',
                            component: downloadArea,
                        },
                        {
                            path: '/projectML/xiangmuProcess/areaPingjia',//整体区域评价
                            name: 'areaPingjia',
                            component: areaPingjia,
                        },
                    ]
                },
                {
                    path: '/projectML/details',
                    name: 'projectMLdetails',
                    component: projectMLdetails,
                },
                {
                    path: '/projectML/checkList',
                    name: 'checkList',
                    component: checkList,
                },{
                    path: '/projectML/pingjiaManage',
                    name: 'pingjiaManage',
                    component: pingjiaManage,
                },{
                    path: '/projectML/pingjiaRecord',
                    name: 'pingjiaRecord',
                    component: pingjiaRecord,
                },{
                    path: '/projectML/areaPingjia',
                    name: 'areaPingjia',
                    component: areaPingjia,
                },
                {
                    path: '/projectML/dongtaigenzong/visualization',
                    name: 'visualization',
                    component: visualization,
                },{
                    path: '/projectML/dongtaigenzong/cxsfxm',
                    name: 'cxsfxm',
                    component: cxsfxm,
                },
                {
                    path: '/projectML/dongtaigenzong/dsjzbsj',
                    name: 'dsjzbsj',
                    component: dsjzbsj,
                },
                {
                    path: '/projectML/dongtaigenzong/cydsjfx',
                    name: 'cydsjfx',
                    component: cydsjfx,
                },
                {
                    path: '/projectML/dongtaigenzong/hadoop',
                    name: 'hadoop',
                    component: hadoop,
                },
                {
                    path: '/projectML/dynamicInnovation/cxdt',
                    name: 'cxdt',
                    component: cxdt,
                },{
                    path: '/xmpgmx/fzcx',
                    name: 'fzcx',
                    component: fzcx,
                },{
                    path: '/xmpgmx/cyfzPg',//产业发展与创新能力评估
                    name: 'cyfzPg',
                    component: cyfzPg,
                },{
                    path: '/xmpgmx/cggz',//成果跟踪
                    name: 'cggz',
                    component: cggz,
                },{
                    path: '/projectML/tongji',//产业发展与创新能力评估
                    name: 'tongji',
                    component: tongji,
                },{
                    path: '/projectML/fenxi',//区域对比分析
                    name: 'fenxi',
                    component: fenxi,
                },{
                    path: '/projectML/xmpjgl',//项目评价管理-列表页
                    name: 'xmpjgl',
                    component: xmpjgl,
                },{
                    path: '/projectML/pgRecord',//项目评价管理-评估记录
                    name: 'pgRecord',
                    component: pgRecord,
                },{
                    path: '/projectML/qyztpjgl',//项目评价管理-区域整体评价管理
                    name: 'qyztpjgl',
                    component: qyztpjgl,
                },{
                    path: '/projectML/qypgxq',//项目评价管理-区域评估详情
                    name: 'qypgxq',
                    component: qypgxq,
                },{
                    path: '/projectML/xmqd',//项目评价管理-区域评估详情-项目清单
                    name: 'xmqd',
                    component: xmqd,
                },{
                    path: '/projectML/zjdf',//项目评价管理-专家打分
                    name: 'zjdf',
                    component: zjdf,
                },{
                    path: '/projectML/qypjmx',//项目评价管理-专家打分-区域评级明细
                    name: 'qypjmx',
                    component: qypjmx,
                },{
                    path: '/projectML/qyjxmb',//项目评价管理-专家打分-区域绩效目标完成情况表
                    name: 'qyjxmb',
                    component: qyjxmb,
                },{
                    path: '/xmpgmx/qyzbsjgl',//项目管理-企业直报数据管理
                    name: 'qyzbsjgl',
                    component: qyzbsjgl,
                },{
                    path: '/xmpgmx/zbqd',//项目管理-企业直报数据管理
                    name: 'zbqd',
                    component: zbqd,
                },{
                    path: '/xmpgmx/qyzbsjtjt',//项目管理-企业直报数据管理-区域直报数据统计图
                    name: 'qyzbsjtjt',
                    component: qyzbsjtjt,
                },{
                    path: '/xmpgmx/qiyezbsjtjt',//项目管理-企业直报数据管理-企业直报数据统计图
                    name: 'qiyezbsjtjt',
                    component: qiyezbsjtjt,
                },{
                    path: '/achievement/achievementgenzong',//成果跟踪
                    name: 'achievementgenzong',
                    component: achievementgenzong,
                },{
                    path: '/projectML/downloadArea',//下载专区
                    name: 'downloadArea',
                    component: downloadArea,
                },]
        },{
            path:'/dataManage',
            name:'dataSider',
            component: dataSider,
            children:[
                {
                    path: '/dataManage/productList',
                    name: 'productList',
                    component: productList
                },
                {
                    path: '/dataManage/xmlb',//项目名录->项目列表
                    name: 'xmlb',
                    component: xmlb,
                },
                {
                    path: '/dataManage/ProductVerify',
                    name: 'ProductVerify',
                    component: ProductVerify
                },
                {
                    path: '/dataManage/wsxx',//项目名录->项目列表-完善信息
                    name: 'wsxx',
                    component: wsxx,
                },
                {
                    path: '/dataManage/xmxq',//项目名录->项目列表-项目详情
                    name: 'xmxq',
                    component: xmxq,
                },
                {
                    path: '/dataManage/xmxxsh',//项目名录->项目信息审核
                    name: 'xmxxsh',
                    component: xmxxsh,
                },
                {
                    path: '/dataManage/tjxmjdrw',//项目名录->添加项目进度任务
                    name: 'tjxmjdrw',
                    component: tjxmjdrw,
                },
                {
                    path: '/dataManage/zjtx',//在线填报->填报项目进度（企业角色）
                    name: 'zjtx',
                    component: zjtx,
                },
                {
                    path: '/dataManage/AddExpert',//在线填报->填报项目进度（企业角色）
                    name: 'AddExpert',
                    component: AddExpert,
                },
                {
                    path: '/dataManage/fzdxm',//在线填报->负责的项目（企业角色）
                    name: 'fzdxm',
                    component: fzdxm,
                },
                {
                    path: '/dataManage/wjgl',//文件管理
                    name: 'wjgl',
                    component: wjgl,
                },
                {
                    path: '/dataManage/qywjgl',//文件管理-企业
                    name: 'qywjgl',
                    component: qywjgl,
                },
                {
                    path: '/dataManage/qysqgl',//在线填报->负责的项目（企业角色）
                    name: 'qysqgl',
                    component: qysqgl,
                },
                {
                    path: '/dataManage/onlineZhibao',//在线填报->企业直报数据
                    name: 'onlineZhibao',
                    component: onlineZhibao,
                },
                {
                    path: '/dataManage/processData',//在线填报->项目进度汇报数据
                    name: 'processData',
                    component: processData,
                },
                {
                    path: '/dataManage/dataXmml',//在线填报->项目管理
                    name: 'dataXmml',
                    component: dataXmml,
                },
                {
                    path: '/dataManage/cjxm',//在线填报->项目管理->创建项目
                    name: 'cjxm',
                    component: cjxm,
                },
                {
                    path: '/dataManage/tbcggz',//在线填报->项目管理->创建项目
                    name: 'tbcggz',
                    component: tbcggz,
                },
                {
                    path: '/dataManage/shenpiProcessData',//在线填报->数据审批->创建项目
                    name: 'shenpiProcessData',
                    component: shenpiProcessData,
                },
                {
                    path: '/dataManage/details',//在线填报->数据审批->创建项目
                    name: 'details',
                    component: details,
                },


                {
                    path: '/dataManage/dataCggz',//在线填报->成果跟踪
                    name: 'dataCggz',
                    component: dataCggz,
                },
                {
                    path: '/dataManage/yszlsp',//在线填报->成果跟踪
                    name: 'yszlsp',
                    component: yszlsp,
                },
                {
                    path: '/dataManage/ckzl',//数据审批-验收资料审批-查看资料
                    name: 'ckzl',
                    component: ckzl,
                },
                {
                    path: '/dataManage/qiyeyszlsp',//数据审批-验收资料审批-查看资料
                    name: 'qiyeyszlsp',
                    component: qiyeyszlsp,
                },
                {
                    path: '/dataManage/qyjxmbsp',//数据审批-验收资料审批-查看资料
                    name: 'qyjxmbsp',
                    component: qyjxmbsp,
                }
            ]
        },{
            path: '/cxcgjy',
            name: 'Home',
            component: Home,
            children:[
                {
                    path: '/cxcgjy/cxcgsy',
                    name: 'cxcgsy',
                    component: cxcgsy,
                },
                {
                    path: '/cxcgjy/detail',
                    name: 'cxcgjyDetail',
                    component: cxcgjyDetail,
                }
            ]
        },
        {
            path: '/notice',
            name: 'Home',
            component: Home,
            children: [
                {
                    path: '/notice/issue',
                    name: 'noticeIssue',
                    component: notice
                }
            ]
        }

    ],
    // mode: 'history'
});
export default router;
//胡旭修改

router.beforeEach((to,from,next)=>{
    const user = JSON.parse(sessionStorage.getItem('userMessage'));
    if(user == null && to.name !== 'Login'){
        next({
            name: 'Login' // 跳转到登录页
        })
    }else{
        if(to.name=='cxsfxm'){
            sessionStorage.setItem('navTitle','多元数据');
        }else if(to.name=='cxdt'){
            sessionStorage.setItem('navTitle','创新动态');
        }else if(to.name=='xmjdhb'||to.name=='xmjdhb3'){
            sessionStorage.setItem('navTitle','项目管理');
        }else if(to.name=='cggz'){
            sessionStorage.setItem('navTitle','成果跟踪');
        }else if(to.name=='fzcx'){
            sessionStorage.setItem('navTitle','评估分析');
        }else if(to.name=='cxcgsy'){
            sessionStorage.setItem('navTitle','成果推介');
        }else if(to.name=='shenpiProcessData'){
            sessionStorage.setItem('navTitle','数据管理');
        }
        next();
    }

})

