/**
 * Created by zhou on 2019/1/5.
 */
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
    title:'前期管理',
    subTitle:'点位管理',
    isError:'',     //ajax  success:false
    errorMsg:'',    //ajax   报错详情
    updatePassword:false, // 修改密码弹窗
    norse: false,
}

const getters = {    //实时监听state的最新状态
    showTitle(state){    //方法名随意，主要承载变化的title
        return state.title
    },
    showSubTitle(state){    //方法名随意，主要承载变化的subTitle
        return state.subTitle
    },
    showisError(state){
        return state.isError
    },
    showErrormsg(state){
        return state.errorMsg
    },
    showupdatePassword(state){
        return state.updatePassword
    }
}

const mutations = {
    changeTitle(state,title){   //自定义改变state初始值的方法
        this.state.title = title[0]
        this.state.subTitle = title[1]
    },
    showError(state,error){
        console.log(state,error);
        this.state.isError = error[0]
        this.state.errorMsg = error[1]
    },
    updatePasswordChange(state,updatePassword){
        console.log(state,updatePassword);
        this.state.updatePassword = updatePassword;
    },
}

const actions = {
    handleChangeTitle(context,title){   //自定义触发mutations里函数的方法，context与store 实例具有相同方法和属性
        context.commit('changeTitle',title)
    },
    handleShowError(context,error){
        context.commit('showError',error)
    }
}

const store = new Vuex.Store({
    state,
    getters,
    mutations,
    actions
})

export default store
